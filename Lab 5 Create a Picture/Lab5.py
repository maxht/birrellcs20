import pygame

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (92, 179, 255)
BROWN = (139, 69, 19)
YELLOW = (255, 255, 0)
SKY = (10, 232, 255)
LEAVES = (2, 157, 19)
pygame.init()

size = (700, 500)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My game")

done = False

clock = pygame.time.Clock()

while not done:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    screen.fill(SKY)

    pygame.draw.rect(screen, GREEN, [0, 300, 700, 300])
    x_offset = 0
    for i in range(1, 6):
        pygame.draw.rect(screen, BROWN, [100 + x_offset, 260, 20, 50])
        pygame.draw.rect(screen, LEAVES, [95 + x_offset, 245, 30, 30])
        x_offset += 120
    pygame.draw.rect(screen, RED, [250, 130, 150, 200])
    pygame.draw.rect(screen, BROWN, [330, 280, 30, 49])

    x_offset = 0
    for i in range(1, 8):
        pygame.draw.rect(screen, BROWN, [50 + x_offset, 310, 20, 50])
        pygame.draw.rect(screen, LEAVES, [45 + x_offset, 290, 30, 30])
        x_offset += 180

    x_offset = 0
    for i in range(1, 4):
        pygame.draw.rect(screen, BLUE, [265 + x_offset, 160, 30, 30])
        x_offset = x_offset + 46

    x_offset = 0
    for i in range(1, 4):
        pygame.draw.rect(screen, BLUE, [265 + x_offset, 200, 30, 30])
        x_offset = x_offset + 46
    x_offset = 0
    for i in range(1, 4):
        pygame.draw.rect(screen, BLUE, [265 + x_offset, 240, 30, 30])
        x_offset = x_offset + 46

    pygame.draw.ellipse(screen, YELLOW, [20, 20, 100, 100])
    pygame.draw.ellipse(screen, YELLOW, [334, 304, 5, 5])
    pygame.draw.line(screen, BLACK, [0, 400], [701, 400], 50)
    x_offset = 0
    for i in range(50):
        pygame.draw.line(screen, YELLOW, [-5 + x_offset, 400], [15 + x_offset, 400], 5)
        x_offset += 50

    pygame.display.flip()

    clock.tick(0)

pygame.quit()
