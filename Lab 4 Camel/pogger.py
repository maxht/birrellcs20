# Chapter 4 Lab
# Christian Claveria, Max Hameluck-Thibeault
# Car Game Program
import random

# Introduction
print("\nWelcome to Escape Rush"
      "\nYou have stolen a car to make your way across the city."
      "\nThe car salesmen want their car back and are chasing you down!"
      "\nSurvive your city trek and out-drive the car salesmen.")

done = False

# Assigning variables
miles_travelled = 0
car_gas = 53
salesmen_distance = -20
atm_money = 1000
cash = 250
car_damage = 0
atm_exit = False
# Starting the while loop
while not done:

    # Ask user for input
    print("\nWhat is your choice?")
    choice = input("A. Use your ATM."
                   "\nB. Ahead moderate speed."
                   "\nC. Ahead full speed."
                   "\nD. Stop for gas."
                   "\nE. Status check."
                   "\nF. Stop for repairs."
                   "\nQ. Quit. "
                   "\nENTER: ")

    # Quit game
    if choice.lower() == "q":
        done = True

    # Repairs
    elif choice.lower() == "f":
        if car_damage <= 0:
            print("You car is in no need of repair.")
        elif car_damage >= 1:
            if cash <= (100 - car_damage) * 4:
                print()
                print("You didn't have enough for a full repair. It cost", (car_damage * 4), "$.")
                salesmen_distance += random.randrange(11, 18)
                car_gas += (cash // 2)
                cash = 0
            elif cash >= (100 - car_damage) * 4:
                cash -= (car_damage * 4)
                salesmen_distance += random.randrange(11, 18)
                print("You had your car repaired. It cost", car_damage * 4, "$.")
                car_damage = 0
        # Interest
        if atm_money <= 999:
            atm_money += 10
    # Status check
    elif choice.lower() == "e":

        # Print status
        print()
        print("Miles travelled:", miles_travelled, "/ 1000 \nThe salesmen are", (-1 * salesmen_distance),
              "miles behind you.", "\nYou have", cash, "$."
              "\nYou have", car_gas, "litres of fuel left.\nCar damage:", car_damage, "%")
    # Refuel car
    elif choice.lower() == "d":
        # Finding a jerry can
        jerry_can = random.randrange(20)
        if jerry_can == 1:
            print("You found a jerry can! Your car is refuelled.")
            car_gas = 53
        else:
            # Making sure car isn't already full
            if car_gas < 53:
                # Making sure user has enough money
                if cash >= (53 - car_gas) * 2:
                    print()
                    print("You refuelled your car.")
                    salesmen_distance += random.randrange(11, 20)
                    refuel = (53 - car_gas) * 2
                    cash -= refuel
                    print("You put", (refuel // 2), "litres into your car. It cost", refuel, "$.")
                    car_gas = 53

                elif cash <= (53 - car_gas) * 2:
                    print()
                    print("You didn't have enough for a full tank, so you put", cash, "$ in for", (cash // 2), "litres")
                    salesmen_distance += random.randrange(11, 20)
                    car_gas += (cash // 2)
                    cash = 0

                elif car_gas >= 53:
                    print()
                    print("The car is already full of gas.")

        # Finding a wallet
        wallet = random.randrange(30)
        if wallet == 1:
            print()
            print("You found a wallet!")
            wallet_contains = random.randrange(1, 501)
            print("There was", wallet_contains, "$ inside.")
            cash += wallet_contains
            if cash >= 500:
                print("You had too much money on hand so some was deposited into an ATM.")
                atm_money = atm_money + (cash - 500)
                cash = 500
        # Interest
        if atm_money <= 999:
            atm_money += 10
    elif choice.lower() == "b":

        # Randomize miles travelled
        if car_damage <= 49:
            # Randomize miles travelled
            miles = random.randrange(10, 16)
            print()
            print("You travelled", miles, "miles.")
            # Lower fuel
            car_gas -= (miles // 2)
            # Salesmen move forward
            salesmen_distance += random.randrange(7, 15)
            salesmen_distance -= miles
            miles_travelled += miles

        elif car_damage >= 75:
            miles = random.randrange(5, 14)
            print()
            print("You travelled", miles, "miles.")
            # Lower fuel
            car_gas -= (miles // 2)
            # Salesmen move forward
            salesmen_distance += random.randrange(7, 15)
            salesmen_distance -= miles
            miles_travelled += miles

        elif car_damage >= 50:
            miles = random.randrange(6, 15)
            print()
            print("You travelled", miles, "miles.")
            # Lower fuel
            car_gas -= (miles // 2)
            # Salesmen move forward
            salesmen_distance += random.randrange(7, 15)
            salesmen_distance -= miles
            miles_travelled += miles

        # Finding a jerry can
        jerry_can = random.randrange(20)
        if jerry_can == 1:
            print("You found a jerry can! Your car is refuelled.")
            car_gas = 53

        # Finding a wallet
        wallet = random.randrange(30)
        if wallet == 1:
            print()
            print("You found a wallet!")
            wallet_contains = random.randrange(1, 501)
            print("There was", wallet_contains, "$ inside.")
            cash += wallet_contains
            # Maximum cash limit
            if cash >= 500:
                print("You had too much money on hand so some was deposited into an ATM.")
                atm_money = atm_money + (cash - 500)
                cash = 500

        # User crashing
        crash = random.randrange(75)
        if crash == 1:
            car_damage += random.randrange(15, 50)
            salesmen_distance += random.randrange(6, 16)
            print("You collided with another vehicle!")
            fine = random.randrange(125, 500)
            print("You have been forced to pay", fine, "$.")
            if cash <= (fine - 1):
                print("You didn't have enough cash, so the rest came from your ATM.")
                fine -= cash
                cash = 0
                if atm_money <= (fine - cash):
                    print("There wasn't enough in your ATM, so you have gone into debt.")
                    fine -= cash
                    atm_money -= fine
                    cash = 0
            else:
                cash -= fine
        car_damage += 1
        # Interest
        if atm_money <= 999:
            atm_money += 10
    # Ahead full speed
    elif choice.lower() == "c":

        if car_damage <= 49:
            # Randomize miles travelled
            miles = random.randrange(10, 21)
            print()
            print("You travelled", miles, "miles.")
            # Lower fuel
            car_gas -= (miles // 2)

            # Salesmen move forward
            salesmen_distance += random.randrange(6, 17)
            salesmen_distance -= miles
            miles_travelled += miles

        elif car_damage >= 75:
            miles = random.randrange(5, 15)
            print()
            print("You travelled", miles, "miles.")
            # Lower fuel
            car_gas -= (miles // 2)

            # Salesmen move forward
            salesmen_distance += random.randrange(6, 17)
            salesmen_distance -= miles
            miles_travelled += miles

        elif car_damage >= 50:
            miles = random.randrange(7, 18)
            print()
            print("You travelled", miles, "miles.")
            # Lower fuel
            car_gas -= (miles // 2)

            # Salesmen move forward
            salesmen_distance += random.randrange(6, 17)
            salesmen_distance -= miles
            miles_travelled += miles

        # Speeding ticket
        speeding = random.randrange(50)
        if speeding == 1:
            fine = random.randrange(125, 251)
            print("You have been fined", fine, "$ for speeding.")
            cash -= fine
            salesmen_distance += random.randrange(10, 19)
            if cash <= 0:
                print("You couldn't pay the fine and have been arrested!")
                done = True

        # Finding a jerry can
        jerry_can = random.randrange(20)
        if jerry_can == 1:
            print("You found a jerry can! Your car is refuelled.")
            car_gas = 53

        # Finding a wallet
        wallet = random.randrange(30)
        if wallet == 1:
            print()
            print("You found a wallet!")
            wallet_contains = random.randrange(1, 501)
            print("There was", wallet_contains, "$ inside.")
            cash += wallet_contains

        # User crashing
        crash = random.randrange(75)
        if crash == 1:
            car_damage += random.randrange(25, 75)
            salesmen_distance += random.randrange(7, 18)
            print("You collided with another vehicle!")
            fine = random.randrange(250, 750)
            print("You have been forced to pay", fine, "$.")
            if cash <= (fine - 1):
                print("You didn't have enough cash, so the rest came from your ATM.")
                fine -= cash
                cash = 0
                if atm_money <= (fine - cash):
                    print("There wasn't enough in your ATM, so you have gone into debt.")
                    fine -= cash
                    atm_money -= fine
                    cash = 0
            else:
                cash -= fine
        car_damage += random.randrange(2)
        # Interest
        if atm_money <= 999:
            atm_money += 10
    # Withdrawing from ATM
    elif choice.lower() == "a":
        while not atm_exit:
            print("\nWhat would you like to do?")
            atm_input = input("A. CHECK BALANCE\nB. WITHDRAW\nC. DEPOSIT\nD. EXIT\nENTER:")
            if atm_input.lower() == "a":
                print("Your current balance is:", atm_money, "$.")

            elif atm_input.lower() == "b":
                atm_withdrawal = int(input("How much would you like to withdraw?\nENTER:"))
                print("You withdrew", atm_withdrawal, "$.")
                atm_money -= atm_withdrawal
                cash += atm_withdrawal

            elif atm_input.lower() == "c":
                atm_deposit = int(input("How much would you like to deposit?\nENTER:"))
                print("You deposited", atm_deposit, "$.")
                atm_money += atm_deposit
                cash -= atm_deposit

            elif atm_input.lower() == "d":
                "OK, exiting..."
                atm_exit = True
        # Interest
        if atm_money <= 999:
            atm_money += 10
    else:
        print("Please enter a valid option.")
    # Car running out of gas
    if car_gas <= 0:
        print()
        print("Your car ran out of gas!")
        done = True
    elif car_gas <= 10:
        print("Your fuel is very low, refuel now!")
    elif car_gas <= 20:
        print("Your fuel is getting low.")

    # Salesmen catching user
    if salesmen_distance >= 0:
        print("The salesmen caught you!")
        done = True
    if salesmen_distance >= 15:
        print("The salesmen are getting close.")

    if car_damage >= 100:
        print("Your car was damaged beyond repair and broke down.")
        done = True
    elif car_damage >= 75:
        print("Warning: Car damage high.")
    elif car_damage >= 50:
        print("Car is damaged.")

    if cash >= 501:
        print("You had too much money on hand so some was deposited into an ATM.")
        atm_money = atm_money + (cash - 500)
        cash = 500

    # Winning
    if miles_travelled >= 1000:
        print("Congratulations, you escaped the city!")
        input("Press enter to quit...")
        done = True

    atm_exit = False
