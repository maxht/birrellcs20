# Chapter 4 Lab
# Christian Claveria, Max Hameluck-Thibeault
# Car Game Program
import random

# Introduction
print("Welcome to Escape Rush, a dystopian future where gas prices are 2$ per litre."
      "\nYou have stolen a car to make your way across the city."
      "\nThe angry car salesmen want their car back and are chasing you down!"
      "\nSurvive your city trek and out-drive the angry car salesmen.")

done = False

# Assigning variables
miles_travelled = 0
car_gas = 53
salesmen_distance = -20
atm_money = 1000
cash = 250
exit = False
# Starting the while loop
while not done:

    # Ask user for input
    print("\nWhat would you like to do?")
    choice = input("A. Check ATM."
                   "\nB. Ahead moderate speed."
                   "\nC. Ahead full speed."
                   "\nD. Stop for gas."
                   "\nE. Status check."
                   "\nQ. Quit. ")

    # Quit game
    if choice.lower() == "q":
        print("OK.")
        done = True

    # Status check
    elif choice.lower() == "e":

        # Print status
        print()
        print("Miles travelled:", miles_travelled, "\nThe salesmen are", (-1 * salesmen_distance),
              "miles behind you.", "\nYou have", cash // 1, "$."
              "\nYou have", car_gas, "litres of fuel left.")

    elif choice.lower() == "d":
        if car_gas < 53:
            print()
            print("You refuelled your car.")
            salesmen_distance += random.randrange(7, 16)
            refuel = (53 - car_gas)
            refuel *= 2
            cash -= refuel
            print("You put", (53 - car_gas), "litres into your car. It cost", refuel, "$.")
            car_gas = 53

        elif car_gas >= 53:
            print()
            print("The car is already full of gas.")

    elif choice.lower() == "b":

        # Randomize miles travelled
        miles = random.randrange(7, 16)
        print()
        print("You travelled", miles, "miles.")

        # Lower fuel
        car_gas -= (miles // 2)

        # Salesmen move forward
        salesmen_distance += random.randrange(6, 14)
        salesmen_distance += miles_travelled
        miles_travelled += miles

    # Ahead full speed
    elif choice.lower() == "c":

        # Randomize miles travelled
        miles = random.randrange(10, 21)
        print()
        print("You travelled", miles, "miles.")

        # Lower fuel
        car_gas -= (miles // 2)

        # Salesmen move forward
        salesmen_distance += random.randrange(6, 14)
        salesmen_distance -= miles
        miles_travelled += miles

        speeding = random.randrange(50)
        if speeding == 1:
            print("You have been pulled over for speeding.")
            fine = random.randrange(125, 251)
            cash -= fine
            print("You have been fined", fine, "$.")
            if cash <= 0:
                print("You couldn't pay the fine and have been arrested!")
                input()
                done = True

    # Withdrawing from ATM

    if choice.lower() == "a":
        exit = False
        while not exit:
            atm_input = input(
                "\n___________________________________"
                "\n| What would you like to do?      |"
                "\n|                                 |"
                "\n| CHECK BALANCE                   |"
                "\n| WITHDRAW                        |"
                "\n| DEPOSIT                         |"
                "\n| EXIT                            |"
                "\n|                                 |"
                "\n¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯"
                "\n                                   ")
            if atm_input.lower() == "exit":
                print("OK, exiting.")
                exit = True
            if atm_input.lower() == "check balance" or atm_input.lower() == "check" or atm_input.lower() == "balance":
                print("Your balance is", atm_money, "$.")
            elif atm_input.lower() == "withdraw":
                if atm_money <= 0:
                    print("\n___________________________________"
                          "\n| INSUFFICIENT FUNDS              |"
                          "\n|                                 |"
                          "\n|                                 |"
                          "\n|                                 |"
                          "\n|                                 |"
                          "\n|                                 |"
                          "\n|                                 |"
                          "\n¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯"
                          "\n                                   ")
                else:
                    atm_withdrawal = int(input("\n___________________________________"
                                               "\n| How much would you like to      |"
                                               "\n| withdraw?                       |"
                                               "\n|                                 |"
                                               "\n|                                 |"
                                               "\n|                                 |"
                                               "\n|                                 |"
                                               "\n|                                 |"
                                               "\n¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯"
                                               "\n                                   "))
                    if atm_withdrawal >= atm_money + 1:
                        print("\n___________________________________"
                              "\n| INSUFFICIENT FUNDS              |"
                              "\n|                                 |"
                              "\n|                                 |"
                              "\n|                                 |"
                              "\n|                                 |"
                              "\n|                                 |"
                              "\n|                                 |"
                              "\n¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯")
                    else:
                        print("You withdrew", atm_withdrawal, "$.")
                        atm_money -= atm_withdrawal
                        cash += atm_withdrawal
            elif atm_input.lower() == "deposit":
                deposit = input("\n___________________________________"
                                "\n| How much would you like to      |"
                                "\n| Deposit?                        |"
                                "\n|                                 |"
                                "\n|                                 |"
                                "\n|                                 |"
                                "\n|                                 |"
                                "\n|                                 |"
                                "\n¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯")
                if deposit >= cash + 1:
                    print("You don't have enough!")
                else:
                    print("You deposited", deposit, "$.")
                    cash -= deposit
                    atm_money += deposit





    # Car running out of gas
    if car_gas <= 0:
        print()
        print("Your car ran out of gas!")
        input()
        done = True

    elif car_gas <= 20:
        print("Your fuel is getting low.")

    # Finding a jerry can
    jerry_can = random.randrange(20)
    if jerry_can == 1:
        print("You found a jerry can! Your car is refuelled.")
        car_gas = 53

    wallet = random.randrange(30)
    if wallet == 1:
        print()
        print("You found a wallet!")
        wallet_contains = random.randrange(1, 501)
        print("There was", wallet_contains, "$ inside.")
        cash += wallet_contains
    if cash >= 501:
        print("\nYou had too much cash on hand so some was deposited into an ATM.")
        atm_money = atm_money + (cash - 500)
        cash = 500

        # Salesmen catching user/getting close
        if salesmen_distance >= 0:
            print("The salesmen caught you!")
            input()
            done = True
        elif salesmen_distance <= 15:
            print("The car salesmen are getting close!")
