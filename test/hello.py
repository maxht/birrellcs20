done = False
room6 = ["You are in an empty room. there is a door to the north.", 4, None, None, None]
room5 = ["You are in the bedroom. There is a door to the west.", None, None, None, 4]
room4 = ["You are in the main room. There are doors to the north, south, east, and west.", 1, 5, 6, 3]
room3 = ["You are in the bathroom. There is a door to the east.", None, 4, None, None]
room2 = ["You are in the kitchen. There is a door to the east.", None, 1, None, None]
room1 = ["You are in the hallway. There are doors to the west, east, and south.", None, 0, 4, 2]
room0 = ["You are in the porch. There is a door to the west.", None, None, None, 1]
current_room = 0
room_list = [room0, room1, room2, room3, room4, room5, room6]
while not done:
    print()
    print(room_list[current_room][0])
    move = input("Where would you like to go? (N, E, S, W) \nType Q if you want to quit. ")
    if move.lower() == "n":
        next_room = room_list[current_room][1]
        if next_room is None:
            print("You can't go that way.")
        else:
            current_room = next_room

    if move.lower() == "e":
        next_room = room_list[current_room][2]
        if next_room is None:
            print("You can't go that way.")
        else:
            current_room = next_room

    if move.lower() == "s":
        next_room = room_list[current_room][3]
        if next_room is None:
            print("You can't go that way.")
        else:
            current_room = next_room

    if move.lower() == "w":
        next_room = room_list[current_room][4]
        if next_room is None:
            print("You can't go that way.")
        else:
            current_room = next_room

    if move.lower() == "q":
        done = True
