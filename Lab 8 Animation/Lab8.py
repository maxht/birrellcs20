import pygame
import random

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)

pygame.init()

# Set the width and height of the screen [width, height]
size = (700, 500)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("bruh")

# Loop until the user clicks the close button.
done = False


def draw_square(screen, rectx, recty):
    pygame.draw.ellipse(screen, YELLOW, [rectx, recty, 50, 50])


# Used to manage how fast the screen updates
clock = pygame.time.Clock()
snow_list = []
for i in range(50):
    x = random.randrange(0, 700)
    y = random.randrange(0, 500)
    snow_list.append([x, y])
pygame.mouse.set_visible(False)
# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here
    pos = pygame.mouse.get_pos()
    x = pos[0]
    y = pos[1]
    # --- Screen-clearing code goes here
    screen.fill(BLACK)

    # --- Drawing code should go here
    draw_square(screen, x, y)

    if x <= 1:
        x += x

    for i in range(len(snow_list)):

        pygame.draw.circle(screen, WHITE, snow_list[i], 2)

        snow_list[i][1] += 1

        if snow_list[i][1] > 500:
            y = random.randrange(-50, -10)
            snow_list[i][1] = y

            x = random.randrange(0, 700)
            snow_list[i][0] = x
    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(600)

# Close the window and quit.
pygame.quit()
